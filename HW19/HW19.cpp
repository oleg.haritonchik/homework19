#include <iostream>
#include <string>

using namespace std;

class Animal 
{
public:
    virtual void  Voice()
    {
        cout << "Example\n\n";
    }
   /* Animal()
    {
        cout << "Animal constructed\n";
    }*/
private:
    string x;

};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "woof\n";
        
    }
   //Dog()
   // {
   //     cout << "Dog ";
   // }
private:
    string x;

};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "meow\n";
    }
    //Cat()
    //{
    //    cout << "Cat ";
    //}
private:
    string x;

};

class Cow : public Animal
{
public:
    void Voice() override
    {
        cout << "mooo\n";
    }
      //Cow()
      //{
      //    cout << "Cow ";
      //}
private:
    string x;

};

class Crow : public Animal
{
public:
    void Voice() override
    {
        cout << "kaar\n";
    }
      //Crow()
      //{
      //    cout << "Crow ";
      //}
private:
    string x;

};
int main()
{
    const int size= 4;
    Animal T;
    T.Voice();


        //Initialize array
    Animal* P[size];
       
    P[0] = new Cow;
    P[1] = new Cat;
    P[2] = new Dog;
    P[3] = new Crow;

    //Print array

    for (int i = 0; i < size; i++)
    {
      P[i]->Voice();
    }


}

